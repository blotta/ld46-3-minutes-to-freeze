function scr_sec2min_string(argument0) {
	// @description scr_sec2min_string(tl);
	// @param tl time left


	var tl = argument0;

	var m = string(int64(tl / 60));
	var s = string(tl % 60);

	if string_length(s) < 2 {
		s = "0" + s;
	}

	return m + ":" + s;



}
