{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 25,
  "bbox_right": 240,
  "bbox_top": 82,
  "bbox_bottom": 127,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dd0de074-f8c3-4b66-a116-13cc35b88112","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd0de074-f8c3-4b66-a116-13cc35b88112","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":{"name":"6bf002ee-7dd0-42e0-a831-72c5512e89a0","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plane_low","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"dd0de074-f8c3-4b66-a116-13cc35b88112","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c1bc630d-1e59-4fdd-9449-bce521509949","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c1bc630d-1e59-4fdd-9449-bce521509949","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":{"name":"6bf002ee-7dd0-42e0-a831-72c5512e89a0","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plane_low","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"c1bc630d-1e59-4fdd-9449-bce521509949","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0aebdd77-8f63-4972-9584-1e8b1c0ba805","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0aebdd77-8f63-4972-9584-1e8b1c0ba805","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":{"name":"6bf002ee-7dd0-42e0-a831-72c5512e89a0","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plane_low","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"0aebdd77-8f63-4972-9584-1e8b1c0ba805","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12d64754-1128-4ff5-9d46-0a189163cf84","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12d64754-1128-4ff5-9d46-0a189163cf84","path":"sprites/spr_plane_low/spr_plane_low.yy",},"LayerId":{"name":"6bf002ee-7dd0-42e0-a831-72c5512e89a0","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plane_low","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","name":"12d64754-1128-4ff5-9d46-0a189163cf84","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_plane_low","path":"sprites/spr_plane_low/spr_plane_low.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7026adf7-45fd-46bf-895b-bf62372d3209","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd0de074-f8c3-4b66-a116-13cc35b88112","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d6bc2329-7e91-463e-96b1-e8283510c68a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1bc630d-1e59-4fdd-9449-bce521509949","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"295e2c8d-9051-476a-9fa0-4d43ad721fce","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0aebdd77-8f63-4972-9584-1e8b1c0ba805","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c09354da-bebc-4a66-939c-6481b62b6dc3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12d64754-1128-4ff5-9d46-0a189163cf84","path":"sprites/spr_plane_low/spr_plane_low.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 128,
    "yorigin": 127,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_plane_low","path":"sprites/spr_plane_low/spr_plane_low.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6bf002ee-7dd0-42e0-a831-72c5512e89a0","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_plane_low",
  "tags": [],
  "resourceType": "GMSprite",
}