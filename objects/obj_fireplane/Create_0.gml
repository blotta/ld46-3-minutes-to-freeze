
//enum intensity {
//	OUT, // 0
//	LOW, // [0,33)
//	MEDIUM, // [33, 66)
//	LARGE // [66, 100)
//}
// view_wport[0]
//start_intensity = intensity.LOW;

//curr_intensity = start_intensity;

fire_max = 100;
fire_amount = 100;
fire_burn_rate = 0.05; // 0, 0.05, 0.1, 0.2;
curr_heat = 0.1; // 0, 0.1, 0.3, 0.5

heat_radius = 300;
heat_radius_color = make_color_rgb(100, 0, 0);

// GUI
bar_label_w = sprite_get_width(spr_barback);
bar_label_h = 8 + (sprite_get_height(spr_barback) / 2);
bar_label_x = view_wport[0] - bar_label_w - 16;
bar_label_y = 16;

bar_width = sprite_get_width(spr_barback);
bar_height = sprite_get_height(spr_barback) / 2;
bar_x = view_wport[0] - bar_width - 16;
bar_y = bar_label_y + bar_label_h;
high_color = c_orange;
low_color = c_dkgray;