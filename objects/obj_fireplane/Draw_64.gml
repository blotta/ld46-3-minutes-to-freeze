var fill_perc = fire_amount / fire_max;

// bar label
draw_sprite_stretched(spr_barback, 0, bar_label_x, bar_label_y, bar_label_w, bar_label_h);

draw_set_font(fnt_small);
draw_set_halign(fa_left);
draw_text(bar_label_x + 4, bar_label_y + 4, "FIRE");

// bar
draw_sprite_stretched(spr_barback, 0, bar_x, bar_y, bar_width, bar_height);
draw_sprite_stretched_ext(spr_barfill, 0,
	bar_x, bar_y,
	fill_perc * bar_width, bar_height,
	merge_colour(low_color, high_color, fill_perc), 1);