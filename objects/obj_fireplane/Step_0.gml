
//var prev_intensity = curr_intensity;

if (!global.paused) {
	// Calculate new amount
	var new_fire_amount = fire_amount - fire_burn_rate;


	fire_amount = clamp(new_fire_amount, 0, fire_max);


	if (fire_amount <= 0) {
		// out
		sprite_index = spr_plane_out;
		fire_burn_rate = 0;
		curr_heat = 0;
	} else if (fire_amount > 0  && fire_amount <= 33) {
		// low
		sprite_index = spr_plane_low;
		fire_burn_rate = 0.02;
		curr_heat = 0.1;
	} else if (fire_amount > 33  && fire_amount <= 66) {
		// Med
		sprite_index = spr_plane_medium;
		fire_burn_rate = 0.05;
		curr_heat = 0.2;
	} else if (fire_amount > 66  && fire_amount <= fire_max) {
		// Large
		sprite_index = spr_plane_large;
		fire_burn_rate = 0.08;
		curr_heat = 0.35;
	}
		

	// Check player in heat radius
	var p = collision_circle(x, y, heat_radius, obj_player, false, true);
	if p != noone {
		p.warmth += curr_heat;
	}
}
