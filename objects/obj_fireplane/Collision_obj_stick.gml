
if fire_amount > 0 {
	fire_amount += other.fuel_amount;
	audio_play_sound(snd_fuelhit, 1, false);
	instance_destroy(other);
}

