//var w = view_get_wport(view_camera[0]);
//var h = view_get_hport(view_camera[0]);
var w = view_wport[0];
var h = view_hport[0];

if (room == rm_fail) {
	//var w = view_get_wport(view_camera[0]);
	//var h = view_get_hport(view_camera[0]);
	
	draw_set_font(fnt_medium);
	draw_set_halign(fa_center);
	draw_text(w/2, h/2, fail_message);
	//draw_text_ext_color(
	//	300,
	//	view_get_hport(view_camera[0]) / 2,
	//	fail_message, 3, view_get_wport(view_camera[0]),
	//	c_ltgray, c_ltgray, c_ltgray, c_ltgray, 1);
	
} else if (room == rm_win) {
	draw_set_font(fnt_medium);
	draw_set_halign(fa_center);
	draw_text(w/2, h/2, win_message);
	//draw_text_ext_color(
	//	300,
	//	view_get_hport(view_camera[0]) / 2,
	//	win_message, 3, view_get_wport(view_camera[0]),
	//	c_ltgray, c_ltgray, c_ltgray, c_ltgray, 1);
} else if (room == rm_game) {
	draw_set_font(fnt_medium);
	draw_set_halign(fa_center);
	draw_text(w / 2, 16, scr_sec2min_string(time_left));
	
	if (!intro_given) {
		draw_sprite_stretched(spr_empty_textbox, 0, 16, 
			h - 200, w - 32, 184);
		
		draw_set_halign(fa_left);
		
		draw_text_ext(68, h - 180, intro[intro_index], 32,
			w - 180);
	}
}