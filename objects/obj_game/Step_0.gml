
if (room == rm_game) {
	
	// Check failure
	if player.warmth <= 0 {
		// fail
		global.paused = true;
		room_goto(rm_fail);
	}
	
	// time running out
	if !global.paused {
		time_left -= delta_time / 1000000;
	}
	
	// Check win
	if time_left <= 0 {
		global.paused = true;
		room_goto(rm_win);
	}
	
	if (!intro_given) {
		var next = mouse_check_button_pressed(mb_left) || 
			keyboard_check_pressed(vk_space) || keyboard_check_pressed(vk_enter);
		
		if (next) {
			intro_index += 1;
			if intro_index > intro_length - 1 {
				intro_given = true;
				global.paused = false;
			}
		}
	}
	
} else if (room == rm_fail) {
	
}