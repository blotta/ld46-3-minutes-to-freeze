
total_time = 3 * 60; //* 0.1;

if (room == rm_game) {
	player = instance_find(obj_player, 0);
	fireplane = instance_find(obj_fireplane, 0);
	time_left = total_time;
	intro_given = false;
}

intro_index = 0
intro[0] = "Wow! How am I still alive?!";
intro[1] = "It's so cold out here. The only source of heat is the fire from the plane crash.";
intro[2] = "Hopefully they'll respond to my mayday call soon enough."
intro[3] = "Meanwhile, I better keep it alive if I want to survive long enough for the rescue team to arrive";
intro[4] = "I bet I can find a few sticks laying around that can serve as fuel for the fire."
intro_length = array_length_1d(intro);


win_message = "You've managed to stay warm until help arrived!";
fail_message = "You have frozen before help could arrive";

global.paused = true;

