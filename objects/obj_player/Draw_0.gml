draw_self();

if (surface_exists(obj_surf_footprints.surface) && sprite_index == spr_player_walk) {
	
	// where to draw>
	surface_set_target(obj_surf_footprints.surface);

	// what to draw
	draw_sprite_ext(spr_footprint, 0, x, y,
		image_xscale, image_yscale, 
		irandom(180), c_white, 1);
	
	// reset
	surface_reset_target();
}

if (!surface_exists(obj_surf_footprints.surface)) {
	obj_surf_footprints.surface = surface_create(room_width, room_height);
}