var kUp = keyboard_check(ord("W")) || keyboard_check(vk_up);
var kLeft = keyboard_check(ord("A")) || keyboard_check(vk_left);
var kDown = keyboard_check(ord("S")) || keyboard_check(vk_down);
var kRight = keyboard_check(ord("D")) || keyboard_check(vk_right);
//var kThrow = keyboard_check_pressed(vk_space);
var kThrow = mouse_check_button_pressed(mb_left);


//var hInput = keyboard_check(vk_right) - keyboard_check(vk_left);
//var vInput = keyboard_check(vk_down) - keyboard_check(vk_up);

var hInput = kRight - kLeft;
var vInput = kDown - kUp;
var dir = point_direction(0, 0, hInput, vInput);

if (!global.paused) {
	if (hInput != 0 or vInput != 0 ) {
	
		//walking = true;
	
		var moveX = lengthdir_x(spd, dir);
		var moveY = lengthdir_y(spd, dir);
	
		if moveX > 1 {
			spr_right = true;
		}
		if moveX < -1 {
			spr_right = false;
		}

		// Collision
		if place_meeting(x + moveX, y, obj_collision) {
			while !place_meeting(x + sign(moveX), y, obj_collision) {
				x += sign(moveX);
			}
			moveX = 0;
		}
	
		if place_meeting(x, y + moveY, obj_collision) {
			while !place_meeting(x, y + sign(moveY), obj_collision) {
				y += sign(moveY);
			}
			moveY = 0;
		}
	

		// Apply movement
		x += moveX;
		y += moveY;
	
		sprite_index = spr_player_walk;
		
		step_tick += 1;
		if step_tick == floor(sprite_get_speed(spr_player_walk) * 2) {
			step_tick = 0;
			audio_sound_pitch(snd_step, random_range(0.9, 1.2))
			audio_play_sound(snd_step, 1, false);
			
		}
	
	} else {
		sprite_index = spr_player_idle;
		step_tick = 0;
	}

	if spr_right {
		image_xscale = 1;
	} else {
		image_xscale = -1;
	}


	// Throw fuel
	if (!throwing) {
		if (kThrow && stick_count > 0) {
			throwing = true;
			var d = point_direction(x, y, mouse_x, mouse_y);
			with (instance_create_layer(x, y, "Instances", obj_stick)) {
				hspeed = lengthdir_x(other.throw_force, d);
				vspeed = lengthdir_y(other.throw_force, d);
				friction = 0.2;
			}
			stick_count -= 1;
			audio_play_sound(snd_throw, 1, false);
			alarm[0] = room_speed / 4;
		}
	}


	// Update warmth
	warmth = warmth - warmth_lower_rate;

}

warmth = clamp(warmth, 0, warmthmax);
//show_debug_message(warmth);