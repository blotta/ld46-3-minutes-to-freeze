spd = 4

spr_right = true

warmthmax = 100
warmth = warmthmax - 96;
warmth_lower_rate = 0.05;

throwing = false;
throw_force = 10;

//walking = false;

// items
stick_count = 0;

step_tick = 0

// GUI
bar_label_x = 16;
bar_label_y = 16;
bar_label_w = sprite_get_width(spr_barback);
bar_label_h = 8 + (sprite_get_height(spr_barback) / 2);

bar_width = sprite_get_width(spr_barback);
bar_height = sprite_get_height(spr_barback) / 2;
bar_x = 16;
bar_y = bar_label_y + bar_label_h;
warm_color = c_red;
cold_color = c_aqua;

stick_counter_x = 16;
stick_counter_y = bar_y + 16;