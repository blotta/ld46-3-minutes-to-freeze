
// Warmth bar
var fill_perc = warmth / warmthmax;

// bar label
draw_sprite_stretched(spr_barback, 0, bar_label_x, bar_label_y, bar_label_w, bar_label_h);
draw_set_halign(fa_left);
draw_set_font(fnt_small);
draw_text(bar_label_x + 4, bar_label_y + 4, "BODY HEAT");


// bar
draw_sprite_stretched(spr_barback, 0, bar_x, bar_y, bar_width, bar_height);

draw_sprite_stretched_ext(spr_barfill, 0,
	bar_x, bar_y,
	fill_perc * bar_width, bar_height,
	merge_colour(cold_color, warm_color, clamp(fill_perc, 0, 1)), 1);

//// Stick counter
draw_set_font(fnt_medium);
draw_set_halign(fa_left);
draw_text(stick_counter_x, stick_counter_y, "Sticks: " + string(stick_count));